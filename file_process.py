import pandas as pd



def read_file(file_path, file_type):
    if file_type == 'csv':
        df = pd.read_csv(file_path)
        return df
    else:
        return None
    

def get_rows_count(df):
    if df is not None:
        shape_info = df.shape
        print(type(shape_info))
        print(shape_info)
        # (2,3)
        return shape_info[0]
    else:
        return None
    


def get_columns_count(df):
    if df is not None:
        shape_info = df.shape
        print(type(shape_info))
        print(shape_info)
        return shape_info[1]
    else:
        return None



if __name__ == '__main__':
    file_path = '/home/sakthi/empolyee.csv'
    df = read_file(file_path, 'csv')
    print(df)
    print(get_columns_count(df))
    print(get_rows_count(df))

