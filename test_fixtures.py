# example 1
import pytest

@pytest.fixture
def input_value():
   input = 39
   return input

def test_divisible_by_3(input_value):

   # 39%3=13
   #assert 0==0

   assert input_value % 3 == 0

def test_divisible_by_6(input_value):
# 39%6==6.3
# assert 3==0

   assert input_value % 6 == 0


