#PyTest is a testing framework that allows users to write test codes using Python programming language#

#Assert Pytest#
# Assertions are simply boolean expressions that check if the conditions return true or not.
# If it is true, the program does nothing and moves to the next line of code. However, if it's false, the program stops and throws an error.


#Example 1

from calcu import *


def test_add():
    add_output = add(2,3)
    assert add_output == 5


def test_add_str():
    add_output = add('anura', 'pari')
    assert add_output == 'anurapari'   


def test_sub():
    sub_output = sub(4, 2)
    #3==2
    assert sub_output==2

def test_mul():
    mul_output = mul(5, 2)
    assert mul_output==10


def test_div():
    div_output = div(10, 2)
    assert div_output==5
        
# exaple_2

def test_rect():
    output = rect(2,5)
    assert output == 10


def test_perirect():
    output = perirect(2, 5)
    assert output == 14



# Execute PyTest Tests from Command Line #
# pytest #
    
#example 3
 
# how to test the csv files#

