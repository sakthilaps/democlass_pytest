from even_num import is_even

def test_is_even():
    assert is_even(2) == True
    
def test_is_even_large_numbers():
    
    assert is_even(999999) == False




