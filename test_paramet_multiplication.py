import pytest

@pytest.mark.parametrize("num, output",[(1,11),(2,22),(3,35),(4,44)])
def test_multiplication_11(num, output):
   
   # 11 * 1 = 11( 11==11)
   # 11 * 2 =22 (22==22)
   # 11 * 3 =33 (33==33)

   assert 11*num == output