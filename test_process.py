import pytest
import pandas as pd
import file_process

@pytest.fixture
def get_df():
    file_path = '/home/sakthi/empolyee.csv'
    df  = pd.read_csv(file_path)
    return df
    
#print(get_df())

def test_get_rows_count(get_df):
    row_counts = file_process.get_rows_count(get_df)

    expected_rows_count = 2

    assert row_counts == expected_rows_count


def test_get_columns_count(get_df):
    columns_count = file_process.get_columns_count(get_df)

    expected_columns_count = 3

    assert columns_count == expected_columns_count

